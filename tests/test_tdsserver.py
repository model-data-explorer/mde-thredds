# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the TDSServer model in thredds-control-center models
-------------------------------------------
"""
from __future__ import annotations

from typing import TYPE_CHECKING, Any, Callable

import pytest

from tds_control import models
from tds_control.tasks import restart_server

if TYPE_CHECKING:
    import requests


def test_tdsserver_internal_url(tdsserver: models.TDSServer):
    """Test the internal URL of the TDSServer."""
    # Get the internal URL
    internal_url = tdsserver.internal_url

    # Assert that the internal URL is not empty
    assert internal_url.strip()


def test_tdsserver_public_url(tdsserver: models.TDSServer):
    """Test the public URL of the TDSServer."""
    # Get the public URL
    public_url = tdsserver.public_url

    # Assert that the public URL is not empty
    assert public_url.strip() != ""


@pytest.mark.parametrize(
    "attr,value",
    [
        ("name", "new_server"),
        ("cors_activation", False),
        ("cors_maxAge", 3600),
        ("cors_allowed_methods", "POST"),
        ("cors_allowed_headers", "Content-Type, Authorization"),
        ("cors_allowed_origin", "https://example.com"),
        ("netcdf_subset_service_activation", True),
        ("netcdfsubsetservice_scour", "5 min"),
        ("netcdfsubsetservice_maxAge", "24 hours"),
        ("opendap_activation", True),
        ("opendap_ascLimit", 100),
        ("opendap_binLimit", 1000),
        ("wcs_activation", False),
        ("wcs_allowRemote", True),
        ("wcs_scour", "10 min"),
        ("wcs_maxAge", "1 hour"),
        ("wms_activation", True),
        ("wms_allowRemote", False),
        ("wms_max_image_width", 1024),
        ("wms_max_image_height", 768),
        ("ncml_allow", False),
        ("uddc_allow", True),
        ("iso_allow", True),
    ],
)
def test_tdsserver_all_fields(
    tds_rf: Callable[[str], requests.Response],
    tdsserver: models.TDSServer,
    attr: str,
    value: Any,
):
    """Test the TDSServer model with different field values."""

    # Create a TDSCatalog
    catalog = models.TDSCatalog.objects.get_or_create(name="test_catalog")[0]
    tdsserver.catalogs.add(catalog)

    # Set different values for the fields
    setattr(tdsserver, attr, value)

    tdsserver.save()

    # Restart the server to make sure configs have changed
    restart_server()

    # Test accessing the generated catalog page
    response = tds_rf(f"thredds/catalog/catalogs/{catalog.name}.html")

    # Assert that the response status code is 200 (OK)
    assert response.status_code == 200
