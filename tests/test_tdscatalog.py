# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the TDSCatalog model in thredds-control-center models
-------------------------------------------
"""
from __future__ import annotations

from typing import TYPE_CHECKING, Any, Callable

import pytest
from django.conf import settings

from tds_control import models
from tds_control.tasks import restart_server

if TYPE_CHECKING:
    import requests


def test_empty_tdscatalog_rendering(
    tds_rf: Callable[[str], requests.Response], tdsserver: models.TDSServer
):
    """Test creating a catalog."""
    catalog = models.TDSCatalog.objects.get_or_create(
        name="empty_test_catalog"
    )[0]
    tdsserver.catalogs.add(catalog)

    # restart the server to make sure configs have changed
    restart_server()

    # test the generated catalog page
    response = tds_rf("thredds/catalog/catalogs/empty_test_catalog.html")
    assert response.status_code == 200


def test_tdscatalog_update_and_rendering(
    tds_rf: Callable[[str], requests.Response], tdsserver: models.TDSServer
):
    """Test updating and rendering a TDSCatalog."""
    catalog = models.TDSCatalog.objects.create(
        name="update_test_catalog", odap=False
    )
    tdsserver.catalogs.add(catalog)

    # Make changes to the catalog
    catalog.odap = True
    catalog.save()

    # Restart the server to apply changes
    restart_server()

    # Test the updated catalog page
    response = tds_rf("thredds/catalog/catalogs/update_test_catalog.html")
    assert response.status_code == 200


def test_render_xml_method_and_server_response(tdsserver: models.TDSServer):
    """Test the render_xml method of TDSCatalog and server response."""
    catalog, created = models.TDSCatalog.objects.get_or_create(
        name="test_catalog"
    )
    tdsserver.catalogs.add(catalog)

    xml_content = catalog.render_xml()
    assert "<?xml" in xml_content


def test_xml_path_property_and_server_response(
    tds_rf: Callable[[str], requests.Response], tdsserver: models.TDSServer
):
    """Test the xml_path property of TDSCatalog and server response."""
    catalog, created = models.TDSCatalog.objects.get_or_create(
        name="test_catalog", resolver=True
    )
    tdsserver.catalogs.add(catalog)

    expected_path = (
        settings.TDS_CONFIG_DIR
        / "catalogs"
        / "test_catalog_enhancedCatalog.xml"
    )
    assert catalog.xml_path == expected_path

    restart_server()

    response = tds_rf(
        "thredds/catalog/catalogs/test_catalog_enhancedCatalog.html"
    )
    assert response.status_code == 200


def test_write_xml_method(tdsserver: models.TDSServer):
    """Test the write_xml method of TDSCatalog and server response."""
    tmp_path = settings.TDS_CONFIG_DIR
    catalog, created = models.TDSCatalog.objects.get_or_create(
        name="test_catalog"
    )
    tdsserver.catalogs.add(catalog)

    catalog.write_xml()
    xml_file = tmp_path / "catalogs" / "test_catalog.xml"
    assert xml_file.exists()


def test_str_method(tdsserver: models.TDSServer):
    """Test the string representation of TDSCatalog and server response."""
    catalog, created = models.TDSCatalog.objects.get_or_create(
        name="test_catalog", resolver=True
    )
    tdsserver.catalogs.add(catalog)

    assert str(catalog) == "test_catalog_enhancedCatalog"


@pytest.mark.parametrize(
    "attr,value",
    [
        ("tds4", True),
        ("resolver", False),
        ("odap", True),
        ("dap4", True),
        ("http", True),
        ("wcs", True),
        ("wms", True),
        ("ncssGrid", True),
        ("ncssPoint", True),
        ("cdmremote", True),
        ("iso", True),
        ("ncml", True),
        ("uddc", True),
    ],
)
def test_tdscatalog_all_fields_usage(
    tds_rf: Callable[[str], requests.Response],
    tdsserver: models.TDSServer,
    attr: str,
    value: Any,
):
    """Test creating a TDSCatalog instance with all fields and verifying server response."""
    catalog = models.TDSCatalog.objects.create(
        name="full_test_catalog", **{attr: value}
    )
    tdsserver.catalogs.add(catalog)

    # Test the XML rendering
    xml_content = catalog.render_xml()
    assert "<?xml" in xml_content

    # Write the XML to file
    catalog.write_xml()
    xml_file = catalog.xml_path
    assert xml_file.exists()
    # Restart the server to apply changes
    restart_server()

    # Test the server response
    response = tds_rf("thredds/catalog/catalogs/full_test_catalog.html")
    assert response.status_code == 200

    # Test the string representation
    expected_str = (
        "full_test_catalog_enhancedCatalog"
        if catalog.resolver
        else "full_test_catalog"
    )
    assert str(catalog) == expected_str
