# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""App settings
------------

This module defines the settings options for the
``thredds-control-center`` app.
"""


from __future__ import annotations

from pathlib import Path

from django.conf import settings  # noqa: F401

TDS_TOMCAT_URL: str = getattr(
    settings, "TDS_TOMCAT_URL", "http://localhost:8080/"
)

if not TDS_TOMCAT_URL.endswith("/"):
    TDS_TOMCAT_URL += "/"

TDS_TOMCAT_PUBLIC_URL: str = getattr(
    settings, "TDS_TOMCAT_PUBLIC_URL", TDS_TOMCAT_URL
)

if not TDS_TOMCAT_PUBLIC_URL.endswith("/"):
    TDS_TOMCAT_PUBLIC_URL += "/"

TDS_MANAGER_USER: str = getattr(settings, "TDS_MANAGER_USER", "manager")
TDS_MANAGER_PASSWORD: str = getattr(
    settings, "TDS_MANAGER_PASSWORD", "password"
)

TDS_CONTENT_ROOT_PATH: Path = Path(
    getattr(settings, "TDS_CONTENT_ROOT_PATH", "/usr/local/tomcat/content")
)

TDS_CONFIG_DIR: Path = Path(
    getattr(settings, "TDS_CONFIG_DIR", TDS_CONTENT_ROOT_PATH / "thredds")
)

TDS_CONTENT_DIR: Path = Path(
    getattr(settings, "TDS_CONTENT_DIR", TDS_CONFIG_DIR / "public")
)
