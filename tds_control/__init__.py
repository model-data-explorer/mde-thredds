# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""THREDDS Control Center

A Django-App to manage a THREDDS Server
"""

from __future__ import annotations

from . import _version

__version__ = _version.get_versions()["version"]

__author__ = "Mostafa Hadizadeh, Philipp S. Sommer, Björn Saß"
__copyright__ = "2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH"
__credits__ = [
    "Mostafa Hadizadeh",
    "Philipp S. Sommer",
    "Björn Saß",
]
__license__ = "EUPL-1.2"

__maintainer__ = "Mostafa Hadizadeh, Philipp S. Sommer, Björn Saß"
__email__ = "mostafa.hadizadeh@kit.edu, philipp.sommer@hereon.de, bjoern.sass@hereon.de"

__status__ = "Pre-Alpha"
