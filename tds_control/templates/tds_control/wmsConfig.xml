<?xml version="1.0" encoding="UTF-8"?>
{% comment %}
SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC0-1.0
{% endcomment %}
<!DOCTYPE wmsConfig SYSTEM "https://schemas.unidata.ucar.edu/thredds/dtd/ncwms/wmsConfig.dtd">
<!--
Detailed configuration of the WMS service.  This config file can be used to
set default styling parameters for each dataset/variable, and to enable or disable
the GetFeatureInfo operation.

See http://www.resc.reading.ac.uk/trac/myocean-tools/wiki/WmsDetailedConfiguration
for more information.
-->
<wmsConfig>
    <global>
        <!-- These settings apply to all datasets unless overridden below -->

    {% if   wmsconfig.id%}
        <defaults>
            <!-- The global defaults. All elements are mandatory -->

            <allowFeatureInfo>{{wmsconfig.allow_feature_info}}</allowFeatureInfo>
            <defaultColorScaleRange>{{vwmsconfigar.lower_range}} {{wmsconfig.defaults_Upper_range}}</defaultColorScaleRange>
            <defaultPaletteName>{{wmsconfig.colorbar}}</defaultPaletteName>
            <defaultNumColorBands>{{wmsconfig.color_bands}}</defaultNumColorBands>
            <logScaling>{{wmsconfig.log_scaling}}</logScaling>
            <intervalTime>{{wmsconfig.interval_time}}</intervalTime>
        </defaults>
        {% for var in wmsconfig.wmsstandardname_set.all %}
        <standardNames>
            <!-- Use this section to set defaults per standard name -->
            <!-- Units must come from the UDUNITS vocabulary -->
            <standardName name="{{var.name}}" units="{{var.unit}}">
                    <allowFeatureInfo>{{var.allow_feature_info}}</allowFeatureInfo>
                    <defaultColorScaleRange>{{var.lower_range}} {{var.defaults_Upper_range}}</defaultColorScaleRange>
                    <defaultPaletteName>{{var.colorbar}}</defaultPaletteName>
                    <defaultNumColorBands>{{var.color_bands}}</defaultNumColorBands>
                    <logScaling>{{var.log_scaling}}</logScaling>
                    <intervalTime>{{var.interval_time}}</intervalTime>
            </standardName>
            <!-- TODO: how about allowing "*fraction" to map to 0:1? -->
            <!-- TODO: how about allowing multiple standard names to map to the same settings,
                 either through a glob expression or through a list? -->
        </standardNames>
        {% endfor %}
    </global>
    {% else %}
        <defaults>
            <!-- The global defaults. All elements are mandatory -->
            <allowFeatureInfo>true</allowFeatureInfo>
            <defaultColorScaleRange>-50 50</defaultColorScaleRange>
            <defaultPaletteName>rainbow</defaultPaletteName>
            <defaultNumColorBands>20</defaultNumColorBands>
            <logScaling>false</logScaling>
            <intervalTime>false</intervalTime>
        </defaults>

        <standardNames>
            <!-- Use this section to set defaults per standard name -->
            <!-- Units must come from the UDUNITS vocabulary -->
            <standardName name="sea_water_potential_temperature" units="K">
                <defaultColorScaleRange>268 308</defaultColorScaleRange>
            </standardName>
            <standardName name="sea_water_temperature" units="K">
                <defaultColorScaleRange>268 308</defaultColorScaleRange>
            </standardName>
            <standardName name="mass_concentration_of_chlorophyll_in_sea_water" units="kg m-3">
                <logScaling>true</logScaling>
            </standardName>
            <!-- TODO: how about allowing "*fraction" to map to 0:1? -->
            <!-- TODO: how about allowing multiple standard names to map to the same settings,
                 either through a glob expression or through a list? -->
        </standardNames>
    </global>
    {% endif %}
    <overrides>

  {% if wmsconfig.id %}
        {% for file in wmsconfig.wmsoverride_set.all %}
        {% if file.directory %}
        <datasetPath pathSpec="{{ file.directory }}">
        {% else %}
        <datasetPath pathSpec="{{ file.single_file }}">
        {% endif %}
            <!-- Will apply to all paths that match the path spec above -->
            <pathDefaults>
                <!-- These will apply to all variables in this path unless overridden below -->
                <allowFeatureInfo>{{file.allow_feature_info}}</allowFeatureInfo>
                <defaultColorScaleRange>{{file.lower_range}} {{file.defaults_Upper_range}}</defaultColorScaleRange>
                <defaultPaletteName>{{file.colorbar}}</defaultPaletteName>
                <defaultNumColorBands>{{file.color_bands}}</defaultNumColorBands>
                <logScaling>{{file.log_scaling}}</logScaling>
                <intervalTime>{{file.interval_time}}</intervalTime>

            </pathDefaults>
            {% for var in file.wmsvariable_set.all %}
            <variables>
                <!-- Configure variables individually according to their internal ID.
                     This is the most specific setting and will override any others -->
                <variable id="{{ var.name }}">
                    <allowFeatureInfo>{{var.allow_feature_info}}</allowFeatureInfo>
                    <defaultColorScaleRange>{{var.lower_range}} {{var.defaults_Upper_range}}</defaultColorScaleRange>
                    <defaultPaletteName>{{var.colorbar}}</defaultPaletteName>
                    <defaultNumColorBands>{{var.color_bands}}</defaultNumColorBands>
                    <logScaling>{{var.log_scaling}}</logScaling>
                    <intervalTime>{{var.interval_time}}</intervalTime>
                </variable>
            </variables>
            {% endfor %}
        </datasetPath>
        {% endfor %}

  {% endif %}
    </overrides>

</wmsConfig>
