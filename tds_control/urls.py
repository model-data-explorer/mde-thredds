# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""URL config
----------

URL patterns of the thredds-control-center to be included via::

    from django.urls import include, path

    urlpatters = [
        path(
            "thredds-control-center",
            include("tds_control.urls"),
        ),
    ]
"""
from __future__ import annotations

from typing import Any

from django.conf import settings
from django.conf.urls.static import static
from django.urls import path  # noqa: F401

from tds_control import views  # noqa: F401

#: App name for the thredds-control-center to be used in calls to
#: :func:`django.urls.reverse`
app_name = "tds_control"

#: urlpatterns for thredds-control-center
urlpatterns: list[Any] = [
    # add urls patterns for thredds-control-center
]

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT
    )
