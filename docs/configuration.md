<!--
SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

(configuration)=

# Configuration options

## Configuration settings

The following settings in your djangos `settings.py` have an effect on the app:

```{eval-rst}
.. automodulesumm:: tds_control.app_settings
    :autosummary-no-titles:
```
