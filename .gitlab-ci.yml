# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip

.python_setup:
  image: python:3.9
  before_script:
    - python -m pip install -U pip

.dedicated-runner: &dedicated-runner
  tags:
  - dind

build-package:
  extends: .python_setup
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    GIT_SUBMODULE_DEPTH: 1
  script:
    - pip install build twine
    - make dist
  artifacts:
    name: python-artifacts
    paths:
      - "dist/*"
    expire_in: 7 days

lint:
  extends: .python_setup
  stage: test
  script:
    - make dev-install
    - make lint

test-package:
  extends: .python_setup
  stage: test
  needs:
    - build-package
  script:
    - pip install twine
    - twine check dist/*
  artifacts:
    name: python-artifacts
    paths:
      - "dist/*"
    expire_in: 7 days

test:
  <<: *dedicated-runner
  stage: test
  image: docker:20.10
  services:
    - docker:20.10-dind
  needs:
    - build-package
    - lint
  script:
    - docker compose -f docker-compose.test.yml build
    - docker compose -f docker-compose.test.yml -f .gitlab-ci/docker-compose-codebase-network.yml up --exit-code-from django-test
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'

test-docs:
  extends: .python_setup
  stage: test
  script:
    - make dev-install
    - make -C docs html
    - make -C docs linkcheck
  artifacts:
    paths:
    - docs/_build


deploy-package:
  extends: .python_setup
  stage: deploy
  needs:
    - test-package
    - test-docs
    - test
  only:
    - main
  script:
    - pip install twine
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
